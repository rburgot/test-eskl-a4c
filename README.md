# test-ESKL-A4C
You need to set the correct host and port in the Dockerfile, then run the following commands:

    docker build -t=push_to_esk .
    docker run --network="host" -t push_to_esk:latest

# if you don't have elastic and kibana, goto subdirectory dockerESK