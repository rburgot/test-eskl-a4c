FROM python:3.7.1
WORKDIR /code
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
# TODO set correct value here
ENV ES_HOST localhost
# TODO set correct value here
ENV ES_PORT 9200
CMD ["python", "main.py"]