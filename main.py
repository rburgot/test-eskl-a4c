from elasticsearch import Elasticsearch, TransportError
from elasticsearch.helpers import bulk
import numpy as np
import time
import os
import datetime

NB_MSG = int(1e5)
INDEX_NAME = "test-a4c"


def iterate_data_to_push(index_name, nb_msg, my_random_id):
    for i in range(nb_msg):
        yield dict(
            _index=index_name,
            _type="random_msg",
            _id=int((time.time() * int(1e7)) % int(1e12)) * 1000 + np.random.randint(999),
            _source=dict(
                my_random_id=my_random_id, timestamp=datetime.datetime.now().isoformat()
            ),
        )


if __name__ == "__main__":
    es = Elasticsearch(
        hosts=[{"host": os.environ["ES_HOST"], "port": int(os.environ["ES_PORT"])}]
    )
    bulk(es, iterate_data_to_push(INDEX_NAME, NB_MSG, np.random.randint(1e6)))
